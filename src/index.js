import 'core-js/es/array';
import 'core-js/es/string/includes';
import 'core-js/es/string/starts-with';
import 'core-js/es/weak-set';
import React from 'react';

import 'react-app-polyfill/ie11';
import { render } from 'react-dom';
import { IntlProvider } from 'react-intl';
import posed, { PoseGroup } from 'react-pose';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import store from 'redux/index';
import registerServiceWorker from 'registerServiceWorker';

// import 'styles/src/app.scss';
import defaultIntl from 'tools/i18n/en-nz.json';

// import SecureRoute from './views/enhancers/SecureRoute';
// import RedirectRoute from './views/enhancers/RedirectRoute';

import Home from 'views/pages/Home/Home';
import Maintenance from 'views/pages/Maintenance/Maintenance';
import NotFound from 'views/pages/NotFound/NotFound';

const RouteContainer = posed.div({
  enter: {
    opacity: 1,
    scale: 1,
    transition: {
      default: { ease: [0.77, 0.0, 0.175, 1.0], duration: 400 },
      opacity: { ease: 'linear', duration: 350, delay: 50 },
    },
  },
  exit: {
    opacity: 0,
    scale: 1.05,
    transition: {
      default: { ease: [0.77, 0.0, 0.175, 1.0], duration: 350, delay: 50 },
      opacity: { ease: 'linear', duration: 400 },
    },
  },
});

render(
  <Provider store={store}>
    <IntlProvider locale="en-NZ" messages={defaultIntl}>
      <Router>
        <Route
          render={({ location }) => (
            <PoseGroup>
              <RouteContainer key={location.pathname}>
                <Switch location={location}>
                  <Route exact path="/" component={Home} />

                  <Route component={NotFound} />
                  <Route component={Maintenance} />
                </Switch>
              </RouteContainer>
            </PoseGroup>
          )}
        />
      </Router>
    </IntlProvider>
  </Provider>,
  // eslint-disable-next-line no-undef
  document.getElementById('root'),
);

registerServiceWorker();
