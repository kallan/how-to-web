import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';

const PageNotFound = () => (
  <>
    <p>
      <FormattedMessage id="404" />
    </p>
  </>
);

export default withRouter(PageNotFound);
