import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';

const Maintenance = () => (
  <>
    <p>
      <FormattedMessage id="SITE_DOWN_FOR_MAINTENANCE" />
    </p>
  </>
);

export default withRouter(Maintenance);
