import { connect } from 'react-redux';

import { toggleMenu } from 'redux/modules/app/actions';

import { Footer } from 'views/components/Footer/Footer';

const mapDispatchToProps = (dispatch) => ({
  toggleMenu(activeContent, heading) {
    return dispatch(toggleMenu(activeContent, heading));
  },
});

const FooterConnected = connect(null, mapDispatchToProps)(Footer);

export { FooterConnected as Footer };
