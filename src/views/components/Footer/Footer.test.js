import axe from 'axe-core';
import toJson from 'enzyme-to-json';
import React from 'react';
import { injectIntl } from 'react-intl';

import { shallowWithIntl } from 'tools/utilities/enzymeIntlTestHelper';
import mountToDoc from 'tools/utilities/mountToDoc';

import { Footer } from 'views/components/Footer/Footer';

const FooterWithIntl = injectIntl(Footer);

const setup = (render, props) => {
  const defaultProps = {};

  const component = render(<FooterWithIntl {...defaultProps} {...props} />);

  return {
    actual: component,
  };
};

describe('<Footer />', () => {
  it('renders correctly', () => {
    const { actual } = setup(shallowWithIntl, {});
    expect(toJson(actual)).toMatchSnapshot();
  });

  it('has no accessibility violations', (done) => {
    const { actual } = setup(mountToDoc, {});
    const componentNode = actual.getDOMNode();

    axe.run(componentNode, (err, { violations }) => {
      expect(err).toBe(null);
      expect(violations).toHaveLength(0);
      done();
    });
  });
});
