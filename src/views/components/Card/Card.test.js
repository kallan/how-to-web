import axe from 'axe-core';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

import mountToDoc from 'tools/utilities/mountToDoc';

import { Card } from 'views/components/Card/Card';

const setup = (render) => {
  const component = render(<Card>Card text </Card>);

  return {
    actual: component,
  };
};

describe('<Card />', () => {
  it('renders correctly', () => {
    const { actual } = setup(shallow, {});
    expect(toJson(actual)).toMatchSnapshot();
  });

  it('has no accessibility violations', (done) => {
    const { actual } = setup(mountToDoc, {});
    const componentNode = actual.getDOMNode();

    axe.run(componentNode, (err, { violations }) => {
      expect(err).toBe(null);
      expect(violations).toHaveLength(0);
      done();
    });
  });
});
