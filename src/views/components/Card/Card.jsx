import PropTypes from 'prop-types';
import React from 'react';

export const Card = ({ className, children }) => (
  <div className={`Card ${className}`}>{children}</div>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Card.defaultProps = {
  className: '',
};
