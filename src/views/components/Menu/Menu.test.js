import axe from 'axe-core';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import { StaticRouter } from 'react-router-dom';

import mockStore from "tools/utilities/mockStore";
import shallowToDoc from "tools/utilities/mountToDoc";

import { Menu } from "views/components/Menu/Menu";

const setup = (render, props) => {
  const handleLogoutSpy = jest.fn();
  const store = mockStore;

  const defaultProps = {
    isOpen: true,
    logOut: handleLogoutSpy,
  };

  const component = render(
    <StaticRouter store={store} context={{}}>
      <Menu {...defaultProps} {...props} />
    </StaticRouter>,
  );

  return {
    actual: component,
    handleLogoutSpy,
  };
};

describe('<Menu />', () => {
  it('renders correctly', () => {
    const { actual } = setup(shallow, {});

    expect(toJson(actual)).toMatchSnapshot();
  });

  it('has no accessibility violations', (done) => {
    const { actual } = setup(shallowToDoc, {});
    const componentNode = actual.getDOMNode();

    axe.run(componentNode, (err, { violations }) => {
      expect(err).toBe(null);
      expect(violations).toHaveLength(0);
      done();
    });
  });
});
