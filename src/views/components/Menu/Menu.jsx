import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { withRouter, Link } from 'react-router-dom';

const Menu = () => (
  <nav className="Menu Menu--sitewide">
    <h2 className="u-hiddenVisually">
      <FormattedMessage id="MENU" />
    </h2>

    <ul className="Menu-list">
      <li className="Menu-list-item">
        <Link to="/shows/10">Menu item</Link>
      </li>
      <li className="Menu-list-item">
        <Link to="/shows/10">Menu item</Link>
      </li>
      <li className="Menu-list-item">
        <Link to="/shows/10">Menu item</Link>
      </li>
    </ul>

    <div className="Hamburger">
      <span />
      <span />
      <span />
      <span />
    </div>
  </nav>
);

Menu.propTypes = {
  history: PropTypes.shape({}),
  location: PropTypes.shape({}).isRequired,
};

Menu.defaultProps = {
  history: {},
};

const MenuWithRouter = withRouter(injectIntl(Menu));

export { MenuWithRouter as Menu };
