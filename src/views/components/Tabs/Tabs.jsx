import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

export const Tabs = ({
  headers, renderActiveTab, className, initialTab,
}) => {
  const getInitialTabIndex = () => {
    let initial = 0;

    if (initialTab !== null || initialTab !== undefined) {
      if (typeof initialTab === 'number') {
        initial = initialTab;
      } else if (typeof initialTab === 'string') {
        const index = headers.findIndex((heading) => heading === initialTab);

        if (index !== -1) {
          initial = index;
        }
      }
    }

    return initial;
  };

  const initialTabIndex = getInitialTabIndex();
  const [activeTabIndex, setActiveTabIndex] = useState(initialTabIndex);

  const handleSetActiveTab = (selectedTabIndex) => {
    if (activeTabIndex === selectedTabIndex) {
      return;
    }

    setActiveTabIndex(selectedTabIndex);
  };

  const activeHeader = headers[activeTabIndex].replace(/\s+/g, '-').toLowerCase();
  const classes = className ? ` ${className}` : '';

  return (
    <ul role="tablist" className="Tabs">
      <div className={`Tab${classes}`}>
        {headers.length >= 1
          && headers.map((header, index) => {
            const formattedHeader = header.replace(/\s+/g, '-').toLowerCase();

            return (
              <li
                className={classNames('Tab-item', { 'is-active': activeTabIndex === index })}
                key={formattedHeader}
                onClick={() => handleSetActiveTab(index)}
                role="presentation"
              >
                <a
                  aria-selected={activeTabIndex === index}
                  className="Link"
                  href={`#${formattedHeader}`}
                  id={`${formattedHeader}-tab`}
                  role="tab"
                >
                  {header}
                </a>
              </li>
            );
          })}
      </div>

      <li aria-labelledby={`${activeHeader}-tab`} id={activeHeader} role="tabpanel">
        {renderActiveTab(activeTabIndex)}
      </li>
    </ul>
  );
};

Tabs.propTypes = {
  className: PropTypes.string,
  headers: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  initialTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  renderActiveTab: PropTypes.func.isRequired,
};

Tabs.defaultProps = {
  className: null,
  initialTab: null,
};
