import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

export const Select = (props) => {
  const {
    baseClassName,
    className,
    error,
    hasOuterLabel,
    IconElement,
    initialSelected,
    isDisabled,
    isLabelHidden,
    label,
    onBlur,
    onChange,
    options,
    placeholder,
    value,
  } = props;

  const [isFocused, toggleFocus] = useState(false);

  const handleOnChange = (event) => {
    if (onChange) {
      onChange(event.target.value);
    }
  };

  const handleOnBlur = (event) => {
    if (onBlur) {
      onBlur(event.target.value);
    }

    toggleFocus(false);
  };

  const handleOnFocus = () => {
    toggleFocus(true);
  };

  const isFloating = !hasOuterLabel && (isFocused || value !== '');

  const fieldClassName = classnames(
    baseClassName,
    className,
    { 'has-error': error },
    { 'has-placeholder': placeholder },
    { 'has-value': value !== '' },
    { 'is-floating': isFloating },
    { 'is-readonly': isDisabled },
  );

  const labelClassName = classnames(`${baseClassName}-label`, {
    'u-hiddenVisually': isLabelHidden,
  });

  return (
    <div className={fieldClassName}>
      <div className={`${baseClassName}-wrap`}>
        <span className={labelClassName}>{label}</span>

        {error && (
          <div className={`${baseClassName}-icon`}>
            {IconElement && <IconElement name="declined" />}
          </div>
        )}

        <select
          onBlur={handleOnBlur}
          onChange={handleOnChange}
          onFocus={handleOnFocus}
          placeholder={placeholder}
          value={value}
        >
          {placeholder && !initialSelected && (
            <option value="" disabled>
              {placeholder}
            </option>
          )}

          {initialSelected && !placeholder && (
            <option value={initialSelected.value}>{initialSelected.label}</option>
          )}

          {options.map((option) => (
            <option disabled={option.disabled} key={`${option.value}`} value={option.value}>
              {option.label}
            </option>
          ))}
          {
            // @HACK to stop iOS truncating option text
          }
          <optgroup className="u-hidden" label="" />
        </select>
      </div>

      {error && (
        <div className="Select-error">
          <p>{error}</p>
        </div>
      )}
    </div>
  );
};

Select.propTypes = {
  baseClassName: PropTypes.string,
  className: PropTypes.string,
  error: PropTypes.string,
  hasOuterLabel: PropTypes.bool,
  IconElement: PropTypes.oneOfType([PropTypes.node, PropTypes.element]),
  initialSelected: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }),
  isDisabled: PropTypes.bool,
  isLabelHidden: PropTypes.bool,
  label: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ).isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

Select.defaultProps = {
  baseClassName: 'Select',
  className: '',
  error: null,
  hasOuterLabel: false,
  IconElement: null,
  initialSelected: null,
  isDisabled: false,
  isLabelHidden: false,
  label: '',
  onBlur: null,
  onChange: null,
  placeholder: '',
  value: '',
};
