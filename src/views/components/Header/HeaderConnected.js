import { connect } from 'react-redux';

import { toggleMenu } from 'redux/modules/app/actions';
import { isMenuOpen } from 'redux/modules/app/selectors';

import { Header } from 'views/components/Header/Header';

const mapStateToProps = (state) => ({
  isMenuOpen: isMenuOpen(state),
});

const mapDispatchToProps = (dispatch) => ({
  toggleMenu() {
    return dispatch(toggleMenu());
  },
});

const HeaderConnected = connect(mapStateToProps, mapDispatchToProps)(Header);

export { HeaderConnected as Header };
