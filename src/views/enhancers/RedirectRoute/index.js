import PropTypes from 'prop-types';
import React from 'react';
import { Redirect } from 'react-router';

export const RedirectRoute = ({ to }) => <Redirect to={{ pathname: to }} />;

RedirectRoute.propTypes = {
  to: PropTypes.string.isRequired,
};
