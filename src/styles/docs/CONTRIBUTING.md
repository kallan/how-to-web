[↖︎ Getting Started](../README.md)

# AINT CSS

###### Last Updated: 2/08/2019 - Tony Watson

## Contributing

### Beankstalk

#### Access

If you need access to Gitlab then please email Brendon Price. Push access to the master branch is restricted. Please create a feature branch and submit a code review request via the Gitlab web app.

#### Command Line

    # Clone the repo
    git clone git@gitlab.com:alphero/aintcss.git

    # Install git-flow & initialise the repo
    brew install git-flow && git flow init -d

    # Create a feature branch
    git flow feature start <FEATURE_NAME_HERE>

    # Make changes
    vim ./

    # Publish the changes to Gitlab
    git flow feature publish <FEATURE_NAME_HERE>

#### Request a Code Review

To request visit [/aintcss/branches](https://gitlab.com/alphero/aintcss/-/branches) to find your branch.

Click "Code Review ⬇️" button and add Tony or James as requested reviewers.
