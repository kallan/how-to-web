// NOTE: This is a generated file, nothing you do here matters
// The source is the all.svg file located at 'public/assets/svg-icons/all.svg'
// The script that generates this file is located at tools/svg-generator.js
// To rebuild this file run 'yarn run generate-svgs'
import React from 'react';

export default {
  'btn-play': (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <use xlinkHref="#btn-play" />
    </svg>
  ),
  'loader': (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <use xlinkHref="#loader" />
    </svg>
  ),
};
