// Exports all actions for the module
import types from 'redux/modules/app/types';

// Global
export const toggleMenu = () => ({
  type: types.TOGGLE_TERMS_CONDITIONS_SHEET,
});

// Routing
export const locationChange = (location) => ({
  type: types.FETCH_ADDRESS_SUGGESTIONS,
  payload: {
    location,
  },
});

export default {
  toggleMenu,
  locationChange,
};
